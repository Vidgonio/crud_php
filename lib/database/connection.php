<?php
abstract class Conn
{

    private static $connection;

    public static function getInstance()
    {
        if (is_null(self::$connection)) {
            self::$connection = new \PDO('mysql:host=localhost;port=3306;dbname=alphat', 'root', '');
        }
        return self::$connection;        
    }
}

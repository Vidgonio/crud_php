-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 27-Jul-2019 às 00:38
-- Versão do servidor: 5.7.23
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alphat`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_geral`
--

DROP TABLE IF EXISTS `produtos_geral`;
CREATE TABLE IF NOT EXISTS `produtos_geral` (
  `ID_produtosGeral` int(11) NOT NULL AUTO_INCREMENT,
  `SKU` int(11) NOT NULL,
  `Nome` varchar(45) NOT NULL,
  `Descricao` varchar(300) NOT NULL,
  `preco` float NOT NULL,
  PRIMARY KEY (`ID_produtosGeral`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos_geral`
--

INSERT INTO `produtos_geral` (`ID_produtosGeral`, `SKU`, `Nome`, `Descricao`, `preco`) VALUES
(1, 50, 'Cha Lipton 2lt', 'extrato de ervas', 5.29),
(2, 57, 'Coca Cola 2lt', 'Extrato de noz de cola com gas', 8.99);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

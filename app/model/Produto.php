<?php
class Produto
{
    public static function getall()
    {
        $con = Conn::getInstance();
        $sql = "SELECT * FROM produtos_Geral ORDER BY ID_produtosGeral ASC";
        $sql = $con->prepare($sql);
        $sql->execute();
        $resultado = array();

        if (is_null($resultado)) {
            throw new Exception("Não Foi Encontrado nenhum registro de produtos, por favor insira um produto na aba inserir produtos.");
        }
        while ($row = $sql->fetchObject('Produto')) {
            $resultado[] = $row;
        }
        return $resultado;
    }
}

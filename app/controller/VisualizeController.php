<?php
class VisualizeController
{
    public function index()
    {
        try {
            $produtos = Produto::getall();
            //var_dump($produtos);
            $loader = new \Twig\Loader\FilesystemLoader('app/view');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('visualize.html');
            $parametros = array();
            $parametros['produtos'] = $produtos;
            $conteudo = $template->render($parametros);
            //var_dump($conteudo);
            echo $conteudo;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

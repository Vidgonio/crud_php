<?php
class Core
{
    public function start($urlGet)
    {
        //ajusta o parametro do get para acessar a pagina referida
        /*if (isset($urlGet['metodo'])) {
            $act = $urlGet['metodo'];
        } else {
            $act = 'index';
        }*/
        $act = 'index';
        if (isset($urlGet['pagina'])) {
            $controller = ucfirst($urlGet['pagina'] . 'Controller');
        } else {
            $controller = 'VisualizeController';
        }
        //var_dump($controller);

        //verifica a existencia
        /*if (class_exists((strval($controller)))) {
            $controller = 'ErroController';
        }*/

        call_user_func_array(array(new $controller, $act), array());
    }
}
